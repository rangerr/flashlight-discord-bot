const fs = require('fs');
const Discord = require('discord.js');
const { prefix, token } = require('./config.json');
const { isNullOrUndefined } = require('util');

const client = new Discord.Client({ partials: ['MESSAGE', 'CHANNEL', 'REACTION']});
//const client = new Discord.Client();
client.commands = new Discord.Collection();

fs.readdir('./Commands', (err, files) => {
    if (err) throw err;
    //continue on
	for (const file of files) {
        if (!file.endsWith('.js')) continue;
		const command = require(`./Commands/${file}`);
		client.commands.set(command.name, command);
	}
});

const reactionRoleMessage = {
    channelID: '793983908370710540',
    messageID: '802627841413873724'
}

qotdprocessing = require('./Modules/qotdposts.js');

const emojiToRoles = {'uninitiated': '801207215012446239', 'casual': '802308386800140338','enthusiast': '802345868744851487','hobbyist': '802380498394677288', 'hardcore': '802380520909963274', 'torchgollum': '802380540433924109'}

//client.on("debug", console.log)
client.on("warn", console.log)
client.on('error', console.error);
client.on('unhandledRejection', console.error);
client.on('shardError', error => {
    console.error('A websocket connection encountered an error:', error);
});

client.on('ready', async() => {
    console.log('Ready!');
    console.log(`Logged in as ${client.user.username}`);
    const botSpam = client.channels.cache.find(c => c.id === '794727051865227334');
});

client.on('rateLimit', rateLimitInfo => {
    const botSpam = client.channels.cache.find(c => c.id === '794727051865227334');
    console.log(rateLimitInfo);   
});

client.on('message', async message => {

    if (message.content.startsWith('+QOTD:')) {
        console.log(`Found QOTD string from ${message.member.displayName}`)
        qotdprocessing.run(client, message)
    }

    if (message.content.toLowerCase().startsWith('when is my server cake day?')){
        var dateOptions = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', hour12: true };
        message.channel.send(`You joined this server on ${new Date(message.member.joinedAt).toLocaleDateString("en-US", dateOptions)}`)
    }

    if (!message.content.startsWith(prefix) || message.author.bot) return;

    const args = message.content.slice(prefix.length).split(/ +/);
    const commandName = args.shift().toLowerCase();

    const command = client.commands.get(commandName)
        || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

    if (!command) return;

    if (command.guildOnly && message.channel.type !== 'text') {
        return message.reply('I can\'t execute that command inside DMs.');
    }

    if (command.args && !args.length) {
        let reply = `You didn't provide any arguments, ${message.author}.`;

        if (command.usage) {
            reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
        }

        return message.channel.send(reply);
    }

    try {
        command.execute(client, message, args);
    }
    catch (error) {
        console.error(error);
        message.reply('there was an error trying to execute that command!');
    }

});

client.on('messageReactionAdd', async (reaction, user) => {
    if (reaction.message.partial) {
        console.log('User added a reaction to a message partial.')
        await reaction.message.fetch()
    }

    const botSpam = client.channels.cache.find(c => c.id === '794727051865227334')

    let member = reaction.message.guild.members.cache.get(user.id)

    if (reaction.message.id === reactionRoleMessage.messageID) {

        let roleIDs = Object.values(emojiToRoles)
        
        if (!member.roles.cache.some(role => roleIDs.includes(role.id) )) {

            if(reaction.emoji.name === 'uninitiated') {
                botSpam.send(`\`${user.username}\` reacted with uninitiated.  I have add role ${client.guilds.cache.find(g => g.id ==='793967107116236841').roles.cache.find(r => r.id === `${emojiToRoles['uninitiated']}`)}`)
                member.roles.add(`${emojiToRoles['uninitiated']}`)
            }
            if(reaction.emoji.name === 'casual') {
                botSpam.send(`\`${user.username}\` reacted with casual.  I have add role ${client.guilds.cache.find(g => g.id ==='793967107116236841').roles.cache.find(r => r.id === `${emojiToRoles['casual']}`)}`)
                member.roles.add(`${emojiToRoles['casual']}`)
            }
            if(reaction.emoji.name === 'enthusiast') {
                botSpam.send(`\`${user.username}\` reacted with enthusiast.  I have add role ${client.guilds.cache.find(g => g.id ==='793967107116236841').roles.cache.find(r => r.id === `${emojiToRoles['enthusiast']}`)}`)
                member.roles.add(`${emojiToRoles['enthusiast']}`)
            }
            if(reaction.emoji.name === 'hobbyist') {
                botSpam.send(`\`${user.username}\` reacted with hobbyist.  I have add role ${client.guilds.cache.find(g => g.id ==='793967107116236841').roles.cache.find(r => r.id === `${emojiToRoles['hobbyist']}`)}`)
                member.roles.add(`${emojiToRoles['hobbyist']}`)
            }
            if(reaction.emoji.name === 'hardcore') {
                botSpam.send(`\`${user.username}\` reacted with hardcore.  I have add role ${client.guilds.cache.find(g => g.id ==='793967107116236841').roles.cache.find(r => r.id === `${emojiToRoles['hardcore']}`)}`)
                member.roles.add(`${emojiToRoles['hardcore']}`)
            }
            if(reaction.emoji.name === 'torchgollum') {
                botSpam.send(`\`${user.username}\` reacted with torchgollum.  I have add role ${client.guilds.cache.find(g => g.id ==='793967107116236841').roles.cache.find(r => r.id === `${emojiToRoles['torchgollum']}`)}`)
                member.roles.add(`${emojiToRoles['torchgollum']}`)
            }
        }
    }
});

client.on('messageReactionRemove', async (reaction, user) => {
    if (reaction.message.partial) {
        console.log('User removed a reaction from a message partial.')
        await reaction.message.fetch()
    }
    if (reaction.partial) await reaction.fetch()
    
    const botSpam = client.channels.cache.find(c => c.id === '794727051865227334')

    await reaction.message.guild.members.fetch({user, force: true })
    let member = reaction.message.guild.members.cache.get(user.id)

    let roleIDs = Object.values(emojiToRoles)

    if (reaction.message.id === reactionRoleMessage.messageID) {

        if (reaction.emoji.name === 'uninitiated') {
            if (member.roles.cache.has(`${emojiToRoles['uninitiated']}`)) {
                botSpam.send(`\`${user.username}\` reacted with uninitiated.  I have remove role ${client.guilds.cache.find(g => g.id ==='793967107116236841').roles.cache.find(r => r.id === `${emojiToRoles['uninitiated']}`)}`)
                await member.roles.remove(`${emojiToRoles['uninitiated']}`)
            }
        }
        if (reaction.emoji.name === 'casual') {
            if (member.roles.cache.has(`${emojiToRoles['casual']}`)) {
                botSpam.send(`\`${user.username}\` reacted with casual.  I have remove role ${client.guilds.cache.find(g => g.id ==='793967107116236841').roles.cache.find(r => r.id === `${emojiToRoles['casual']}`)}`)
                await member.roles.remove(`${emojiToRoles['casual']}`)
            }
        }
        if (reaction.emoji.name === 'enthusiast') {
            if (member.roles.cache.has(`${emojiToRoles['enthusiast']}`)) {
                botSpam.send(`\`${user.username}\` reacted with enthusiast.  I have remove role ${client.guilds.cache.find(g => g.id ==='793967107116236841').roles.cache.find(r => r.id === `${emojiToRoles['enthusiast']}`)}`)
                await member.roles.remove(`${emojiToRoles['enthusiast']}`)
            }
        }
        if (reaction.emoji.name === 'hobbyist') {
            if (member.roles.cache.has(`${emojiToRoles['hobbyist']}`)) {
                botSpam.send(`\`${user.username}\` reacted with hobbyist.  I have remove role ${client.guilds.cache.find(g => g.id ==='793967107116236841').roles.cache.find(r => r.id === `${emojiToRoles['hobbyist']}`)}`)
                await member.roles.remove(`${emojiToRoles['hobbyist']}`)
            }
        }
        if(reaction.emoji.name === 'hardcore') {
            if (member.roles.cache.has(`${emojiToRoles['hardcore']}`)) {
                botSpam.send(`\`${user.username}\` reacted with hardcore.  I have remove role ${client.guilds.cache.find(g => g.id ==='793967107116236841').roles.cache.find(r => r.id === `${emojiToRoles['hardcore']}`)}`)
                await member.roles.remove(`${emojiToRoles['hardcore']}`)
            }
        }
        if(reaction.emoji.name === 'torchgollum') {
            if (member.roles.cache.has(`${emojiToRoles['torchgollum']}`)) {
                botSpam.send(`\`${user.username}\` reacted with torchgollum.  I have remove role ${client.guilds.cache.find(g => g.id ==='793967107116236841').roles.cache.find(r => r.id === `${emojiToRoles['torchgollum']}`)}`)
                await member.roles.remove(`${emojiToRoles['torchgollum']}`)
            }
        }
    }
});

client.login(token);