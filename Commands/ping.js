module.exports = {
    name: 'ping',
    description: 'Gives the network ping of the bot.',
    //aliases: [''],
    //usage: '',
    cooldown: 5,
    execute(client, message, args) {
        //message.channel.send('Pong.');

        message.channel.send('Pinging...').then(sent => {
            sent.edit(`Pong! Took ${sent.createdTimestamp - message.createdTimestamp}ms`);
        });
    },
};
