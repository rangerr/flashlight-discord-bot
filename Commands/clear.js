const { prefix } = require('../config.json');

module.exports = {
    name: 'clear',
    description: 'Command used to delete messages from a channel. (doesnt not delete pinned messages)',
    aliases: ['purge'],
    usage: '[number of messages to remove]',
    //args: true,
    //cooldown: 5,
	guildOnly: true,
    async execute(client, message, args) {
			   
		if (message.member.hasPermission('MANAGE_MESSAGES')) {

			let deleteCount = +args[0] + 1;

			if (args.length == 0) {
				deleteCount = 2;
			}

			if (isNaN(deleteCount)) {
				message.channel.send(`You must supply a number. Please try again`)
				return
			}

			message.channel.messages.fetch({limit: deleteCount})
					.then(messages => messages.filter(m => !m.pinned))
					.then(d => message.channel.bulkDelete(d))
					.catch(console.error);
			return
		}
		else {
			//message.channel.send(`Does not have \`MANAGE_MESSAGES\` permssion.`);
			message.react('😢');
		}
    }
}
