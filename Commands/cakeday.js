const { prefix } = require('../config.json');
const Discord = require('discord.js');

module.exports = {
	name: 'cakeday',
	description: 'Check to see if anyone has a cake day today for the server ',
	//aliases: [''],
	usage: '[2 digit dates optional dd/mm - 02/24]',
	//args: true,
	guildOnly: true,
	cooldown: 5,
	async execute(client, message, args) {

        await message.guild.members.fetch();

        const msgchannel = client.channels.cache.find(c => c.id === '794727051865227334'); //#botspam
 
        const modRole = message.guild.roles.cache.find(r => r.name === 'Moderator');

        var dateOptions = { month: '2-digit', day: '2-digit' };

        let today = new Date().toLocaleDateString("en-US", dateOptions)
        let currentYear = new Date().getFullYear()

        if (args.length == 0) {
            membersFiltered = message.guild.members.cache.sort((a, b) => a.joinedAt - b.joinedAt).filter(m => new Date(m.joinedAt).toLocaleDateString("en-US", dateOptions) == today && new Date(m.joinedAt).getFullYear() < currentYear);
            
            sendlist = membersFiltered.map(m1 => `\`${m1.displayName}\` ${m1.user} - ${currentYear - m1.joinedAt.getFullYear()} years old`).join('\n');

            //msgchannel.send(`Variable today \`${today}\``)
            //msgchannel.send(`Variable currentYear \`${currentYear}\``)

            embed = new Discord.MessageEmbed()
                .setDescription(`Server Cake Days today!  [${membersFiltered.size}]`)
                
            if (sendlist.length > 0 && sendlist.toString().length < 1020) {
                embed.addField(`List`, `${sendlist}`)
            }
            if (sendlist.toString().length > 1020) {
                //embed.addField(`List`, `List is too long`)
                let [first, second, ...rest] = Discord.Util.splitMessage(sendlist, { maxLength: 1020 })
                
                embed.addField(`List`, `${first}`)

                if (second.toString().length < 1020 ) {
                    embed.addField(`List 2`, `${second}`)
                }
                
                for (const text of rest) {
                    
                    if (rest.toString().length < 1020 ) {
                        embed.addField(`List 3`, `${rest}`)
                    }
                }
                console.log(`Cakeday command for ${today}\n${sendlist}`)
            }
            message.channel.send(embed)
        }
        
        if (args.length > 0) {
            //msgchannel.send(`Running args date`)
            date = args[0]

            membersFiltered = message.guild.members.cache.sort((a, b) => a.joinedAt - b.joinedAt).filter(m => new Date(m.joinedAt).toLocaleDateString("en-US", dateOptions) == date && new Date(m.joinedAt).getFullYear() < currentYear);

            sendlist = membersFiltered.map(m1 => `\`${m1.displayName}\` ${m1.user} - ${currentYear - m1.joinedAt.getFullYear()} years old`).join('\n');
            
            embed = new Discord.MessageEmbed()
                .setDescription(`Server Cake Days on \`${date}\`!  [${membersFiltered.size}]`)
                
            if (sendlist.length > 0 && sendlist.toString().length < 1020) {
                embed.addField(`List`, `${sendlist}`)
            }
            if (sendlist.toString().length > 1020) {
                //embed.addField(`List`, `List is too long`)
                let [first, second, ...rest] = Discord.Util.splitMessage(sendlist, { maxLength: 1020 })
                
                embed.addField(`List`, `${first}`)

                if (second.toString().length <= 1020 ) {
                    embed.addField(`List 2`, `${second}`)
                }

                for (const text of rest) {
                    if (rest.toString().length <= 1020 ) {
                        embed.addField(`List 3`, `${rest}`)
                    }
                }
                console.log(`Cakeday command for ${args[0]}\n${sendlist}`)
            }
            //console.log(sendlist)
            message.channel.send(embed)
        }
    }   
}