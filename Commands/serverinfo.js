const Discord = require('discord.js');

module.exports = {
    name: 'serverinfo',
    description: 'Display info about this server.',
    aliases: ['server','si'],
    guildOnly: true,
    cooldown: 5,
    execute(client, message, args) {

        message.guild.members.fetch().then(g =>{
            var ServerCreatedDate = new Date(g.createdTimestamp).toLocaleDateString("en-US");
            embed = new Discord.MessageEmbed()
                .setAuthor(g.name, g.iconURL)
                .setThumbnail(g.iconURL)
                .addField('ID', g.id, true)
                .addField('Owner', g.owner, true)
                .addField('Created on', ServerCreatedDate, true)
                .addField('Categories', g.channels.cache.filter(m => m.type === 'category').size, true)
                .addField('Text Channels', g.channels.cache.filter(m => m.type === 'text').size, true)
                .addField('Voice Channels', g.channels.cache.filter(m => m.type === 'voice').size, true)
                .addField('Member Count', g.memberCount, true)
                .addField('People', g.members.cache.filter(m => !m.user.bot).size, true)
                .addField('Bots', g.members.cache.filter(m => m.user.bot).size, true)
                .addField('Roles', g.roles.cache.size, true)
                .addField('Custom emojis', g.emojis.size, true)
                //.addField('', , true)

            message.channel.send(embed);
            //message.channel.send(`Server name: ${g.name}\nTotal members: ${g.memberCount}\nTotal channels: ${g.channels.filter(m => m.type === 'text').size}\nTotal categories: ${g.channels.filter(m => m.type === 'category').size}`);
        }).catch(console.error);
    },
};