const Discord = require('discord.js');


module.exports = {

    async run(client, message) {
        const msgchannel = client.channels.cache.find(c => c.id === '794727051865227334'); //#botspam
        const qotdArchive = client.channels.cache.find(c => c.id === '815394338988621844'); //#qotd-archive

        let qotdQuestion = message.content.substr(1);

        const image = message.attachments.size > 0 ? await this.extension(message.attachments.array()[0].url) : '';

        const embed = new Discord.MessageEmbed()
            .setColor(15844367)
            .setDescription(qotdQuestion)
            .addField('Author', `${message.author}  \`${message.member.displayName}\``, true)
            .setImage(image)
            .setTimestamp()

        const archiveembed = new Discord.MessageEmbed()
            .setDescription(qotdQuestion)
            .addField('Author', `${message.author}  \`${message.member.displayName}\``, true)
            .addField('Channel', `${message.channel}`, true)
            .addField('Original Message', `[Jump To](${message.url})`)
            //.addField('Question', `${qotdQuestion}`)
            .setImage(image)
            .setTimestamp()

        if(message.channel.id !== '794727051865227334') { //#botspam
            qotdArchive.send(archiveembed).then(console.log(`Posted QOTD to archive channel.`))
        }

        await message.channel.send(embed).then(m => m.pin()).then(console.log(`Pinned QOTD message to ${message.channel.name}`))
        await message.channel.messages.fetch({ limit: 2 }).then(msgs => {
            msgs.find(msg => {
                if (msg.type === 'PINS_ADD') {
                    msg.delete({ timeout: 1000 })
                }
            })           
        })

        message.channel.messages.fetchPinned().then(messages => {
            messages.find(msg => {
                if(msg.embeds[0]) {
                    if (msg.embeds[0].description) {
                        if (msg.embeds[0].description.startsWith('QOTD:') && new Date(msg.embeds[0].timestamp).getTime() < message.createdTimestamp) {
                            //message.channel.send(`Found an older pinned qotd, message id: \`${msg.id}\``)
                            msg.unpin()
                        }
                    }
                }
            })
        })
    },

    extension(attachment) {
        const imageLink = attachment.split('.');
        const typeOfImage = imageLink[imageLink.length - 1];
        const image = /(jpg|jpeg|png|gif)/gi.test(typeOfImage);
        if (!image) return '';
        return attachment;
    }
}